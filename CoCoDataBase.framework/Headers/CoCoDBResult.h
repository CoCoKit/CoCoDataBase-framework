//
//  CoCoDBResult.h
//  CoCoDatabase
//
//  Created by 陈明 on 2016/5/28.
//  Copyright © 2016年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    CoCoDatabaseCommandTypeSelect,
    CoCoDatabaseCommandTypeInsert,
    CoCoDatabaseCommandTypeUpdate,
    CoCoDatabaseCommandTypeDelete,
} CoCoDatabaseCommandType;

@interface CoCoDBResult : NSObject

@property (nonatomic, assign) CoCoDatabaseCommandType commandType;
@property (nonatomic, assign) BOOL success;
@property (nonatomic, strong) NSArray *data;
@property (nonatomic, assign) NSUInteger count;
@end
