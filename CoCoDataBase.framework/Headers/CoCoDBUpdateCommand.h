//
//  CoCoDBUpdateCommand.h
//  CoCoDatabase
//
//  Created by 陈明 on 2016/5/28.
//  Copyright © 2016年 CoCo. All rights reserved.
//

#import "CoCoDBCommand.h"

@interface CoCoDBUpdateCommand : CoCoDBCommand
@property (readonly, nonatomic, copy) CoCoDBUpdateCommand * (^ Set)(NSString *column, id value);

@property (readonly, nonatomic, copy) CoCoDBUpdateCommand * (^ Where)(NSString *condition);

@property (readonly, nonatomic, copy) CoCoDBUpdateCommand * (^ And)(NSString *condition);

@property (readonly, nonatomic, copy) CoCoDBUpdateCommand * (^ Or)(NSString *condition);

@property (readonly, nonatomic, copy) CoCoDBUpdateCommand * (^ Between)(id a, id b);

@property (readonly, nonatomic, copy) CoCoDBUpdateCommand *With; //不同限定介词的连接词 分隔作用
@end
