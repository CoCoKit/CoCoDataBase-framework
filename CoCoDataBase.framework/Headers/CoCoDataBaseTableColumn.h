//
//  CoCoDataBaseTableColumn.h
//  CoCoDatabase
//
//  Created by 陈明 on 2016/5/28.
//  Copyright © 2016年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CoCoDatabaseMarco.h"

@interface CoCoDataBaseTableColumn : NSObject
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *propertyType;
@property (nonatomic, assign) CoCoDataBaseDataType type;
@property (nonatomic, assign) BOOL isAutoIncrement;
@property (nonatomic, assign) BOOL isKey;
@property (nonatomic, assign) BOOL isNotNull;

- (instancetype)initWithName:(NSString *)name;
- (instancetype)initWithPRAGMATableInfo:(NSDictionary *)info;
@end
