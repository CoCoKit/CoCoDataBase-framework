//
//  CoCoDBChangeDL.h
//  CoCoDatabase
//
//  Created by 陈明 on 2016/5/28.
//  Copyright © 2016年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CoCoDBChangeDL : NSObject
+ (BOOL)changeMappingForTableClass:(Class)cls;

+ (BOOL)vacuumSpaceForTableClass:(Class)cls;
@end
