//
//  CoCoDatabaseMarco.h
//  CoCoDatabase
//
//  Created by 陈明 on 2016/5/28.
//  Copyright © 2016年 CoCo. All rights reserved.
//

#ifndef CoCoDatabaseMarco
#define CoCoDatabaseMarco

#define CoCoDBTablePrefix  @"CoCo_"
#define CoCoDBColumnPrefix @"coco_"
#define kSystemTable       @"CoCo_System"


typedef enum : NSUInteger {
    CoCoDataBaseDataTypeNULL,
    CoCoDataBaseDataTypeINTEGER,    // 一个带符号的整数，根据值的大小存储在 1、2、3、4、6 或 8 字节中
    CoCoDataBaseDataTypeREAL,       // 浮点值，存储为 8 字节的 IEEE 浮点数字
    CoCoDataBaseDataTypeTEXT,       // 文本字符串，使用数据库编码（UTF-8、UTF-16BE 或 UTF-16LE）存储。
    CoCoDataBaseDataTypeBLOB,       // 自定义大数据对象，存储二进制文件
    CoCoDataBaseDataTypeUnSupport,
} CoCoDataBaseDataType;

#endif /* CoCoDatabaseMarco */
