//
//  NSString+PropertyRegular.h
//  CoCoDatabase
//
//  Created by 陈明 on 2016/5/28.
//  Copyright © 2016年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (PropertyRegular)
- (BOOL)isObject;
- (BOOL)isCGRect;
- (BOOL)isCGPoint;
- (BOOL)isCGSize;
- (BOOL)isUnknow;
- (BOOL)isNSType;
- (BOOL)isCType;
- (BOOL)isUIEdgeInsets;
- (NSString *)nsType;
- (NSString *)cType;
@end
