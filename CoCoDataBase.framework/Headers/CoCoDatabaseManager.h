//
//  CoCoDatabaseManager.h
//  CoCoDataBase
//
//  Created by 陈明 on 2018/2/5.
//  Copyright © 2018年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CoCoDatabaseManager : NSObject
+ (CoCoDatabaseManager *)sharedInstance;
@end
