//
//  NSObject+CoCoDatabaseAdditions.h
//  CoCoDatabase
//
//  Created by 陈明 on 2016/5/28.
//  Copyright © 2016年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CoCoDBSelectCommand, CoCoDBUpdateCommand, CoCoDBInsertCommand, CoCoDBDeleteCommand, CoCoDBResult;

@interface NSObject (CoCoDatabaseAdditions)

+ (CoCoDBResult *)makeSelectCommand:(void (^)(CoCoDBSelectCommand *command))commandBlock;

+ (CoCoDBResult *)makeUpdateCommand:(void (^)(CoCoDBUpdateCommand *command))commandBlock;

+ (CoCoDBResult *)makeInsertCommand:(void (^)(CoCoDBInsertCommand *command))commandBlock;

+ (CoCoDBResult *)makeDeleteCommand:(void (^)(CoCoDBDeleteCommand *command))commandBlock;

+ (BOOL)vacuumSpace;

@end
