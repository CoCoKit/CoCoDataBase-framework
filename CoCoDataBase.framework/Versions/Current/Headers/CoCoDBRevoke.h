//
//  CoCoDBRevoke.h
//  CoCoDatabase
//
//  Created by 陈明 on 2017/3/22.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * 嵌入式数据库权限系统直接走iOS的文件访问权限，无需授权和请求
 */

@interface CoCoDBRevoke : NSObject

@end
