//
//  CoCoDBCommand.h
//  CoCoDatabase
//
//  Created by 陈明 on 2016/5/28.
//  Copyright © 2016年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoCoDataBase/CoCoDBResult.h>
#import <CoCoDataBase/CoCoDatabaseMarco.h>


#define strKey(...)         [NSString stringWithFormat: @"%@", @(((void)(NO && ((void)__VA_ARGS__, NO)), strchr(# __VA_ARGS__, '.') + 1))]
#define strKeyWithPrefix(H) [NSString stringWithFormat:@"%@%@", CoCoDBColumnPrefix, strKey(H)]

#define condition(P, O, V)  [NSString stringWithFormat: @"%@%@ %@ %@", CoCoDBColumnPrefix, strKey(P), O, [V isKindOfClass:NSString.class] ? [NSString stringWithFormat:@"'%@'", V] : V]

@interface CoCoDBCommand : NSObject
@property (nonatomic, strong, readonly) NSMutableString *sql;
@property (nonatomic, strong) Class cls;
- (void)addActionCommand;
- (void)appendSubSQL:(NSString *)subSql;
- (instancetype)initWithClass:(Class)cls;
- (CoCoDBResult *)excuteCommand;
@end
