//
//  CoCoDBHelper.h
//  CoCoDatabase
//
//  Created by 陈明 on 2016/5/28.
//  Copyright © 2016年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface CoCoDBHelper : NSObject
+ (NSString *)tableNameWithClass:(Class)cls;
+ (NSString *)columnNameWithProperty:(NSString *)property;

+ (NSUInteger)getTypeWithSqlType:(NSString *)type;
+ (NSString *)stringWithDate:(NSDate *)date;
+ (NSDateFormatter *)getDBDateFormat;
@end
