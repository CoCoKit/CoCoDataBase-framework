//
//  CoCoDBInsertCommand.h
//  CoCoDatabase
//
//  Created by 陈明 on 2016/5/28.
//  Copyright © 2016年 CoCo. All rights reserved.
//

#import "CoCoDBCommand.h"

@interface CoCoDBInsertCommand : CoCoDBCommand

@property (nonatomic, strong, readonly) NSMutableArray *entitys;

// 添加单个
@property (readonly, nonatomic, copy) CoCoDBInsertCommand * (^ AddItem)(id);

// 添加多个
@property (readonly, nonatomic, copy) CoCoDBInsertCommand * (^ AddItems)(NSArray *items);
@end
