//
//  NSObject+CoCoDatabaseModel.h
//  CoCoDatabase
//
//  Created by 陈明 on 2016/5/28.
//  Copyright © 2016年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FMResultSet;

@interface NSObject (CoCoDatabaseModel)
+ (NSDictionary *)propertysWithClass:(Class)cls;
- (NSDictionary *)propertys;
+ (NSString *)getSqlTypeString:(NSUInteger)type;
+ (NSUInteger)sqliteTypeWithPropertyType:(NSString *)type;
- (id)databaseObjectForKey:(NSString *)key type:(NSString *)type;
- (void)updateWithStatements:(NSDictionary *)statements;
- (void)updateWithFMResultSet:(FMResultSet *)set;
@end
