//
//  CoCoDBDropDL.h
//  CoCoDatabase
//
//  Created by 陈明 on 2016/5/28.
//  Copyright © 2016年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CoCoDBDropDL : NSObject
+ (BOOL)dropTableWithClass:(Class)cls;
@end
