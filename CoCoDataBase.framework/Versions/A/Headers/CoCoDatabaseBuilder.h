//
//  CoCoDatabaseBuilder.h
//  CoCoDatabase
//
//  Created by 陈明 on 2016/5/28.
//  Copyright © 2016年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CoCoDatabaseBuilder : NSObject


// 建立数据库
+ (BOOL)buildWithClasses:(NSSet *)classes;


+ (BOOL)dropWithClasses:(NSSet *)classes;

// 数据库路径
+ (NSString *)dbPath;

+ (BOOL)databaseExitWithPath:(NSString *)path;
@end
