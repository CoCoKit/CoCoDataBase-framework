//
//  CoCoDatabaseProperty.h
//  CoCoDatabase
//
//  Created by 陈明 on 2016/5/28.
//  Copyright © 2016年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CoCoDatabaseProperty : NSObject

/**
 * 默认排除默认属性，只留下自定义属性
 */
+ (NSArray *)propertyListWithClass:(Class)cls;

+ (NSArray *)propertyListWithClass:(Class)cls exceptDefault:(BOOL)exceptDefault;

+ (NSDictionary *)propertyTypeListWithClass:(Class)cls;

+ (NSDictionary *)propertyTypeListWithClass:(Class)cls exceptDefault:(BOOL)exceptDefault;

@end
