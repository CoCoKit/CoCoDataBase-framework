//
//  CoCoDatabaseQueue.h
//  CoCoDatabase
//
//  Created by 陈明 on 2016/5/28.
//  Copyright © 2016年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <FMDB/FMDatabaseQueue.h>
@interface CoCoDatabaseQueue : NSObject
+ (CoCoDatabaseQueue *)sharedInstance;

- (void)buildWithPath:(NSString *)path;

@property (nonatomic, strong) FMDatabaseQueue *dbQueue;
@end
