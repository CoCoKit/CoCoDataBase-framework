//
//  CoCoDBSelectCommand.h
//  CoCoDatabase
//
//  Created by 陈明 on 2016/5/28.
//  Copyright © 2016年 CoCo. All rights reserved.
//

#import "CoCoDBCommand.h"



@interface CoCoDBSelectCommand : CoCoDBCommand

/**
 * 限定词
 */

@property (readonly, nonatomic, strong) CoCoDBSelectCommand * (^ All)(void);

@property (readonly, nonatomic, strong) CoCoDBSelectCommand * (^ Count)(void);

@property (readonly, nonatomic, copy) CoCoDBSelectCommand * (^ Columns)(NSString *column, ...);

@property (readonly, nonatomic, copy) CoCoDBSelectCommand * (^ Page)(NSUInteger page, NSUInteger num);

@property (readonly, nonatomic, copy) CoCoDBSelectCommand * (^ Asc)(NSString *column, ...);

@property (readonly, nonatomic, copy) CoCoDBSelectCommand * (^ Desc)(NSString *column, ...);

/**
 * 介词
 */
@property (readonly, nonatomic, copy) CoCoDBSelectCommand * (^ Where)(NSString *condition);

@property (readonly, nonatomic, copy) CoCoDBSelectCommand * (^ And)(NSString *condition);

@property (readonly, nonatomic, copy) CoCoDBSelectCommand * (^ Or)(NSString *condition);

@property (readonly, nonatomic, copy) CoCoDBSelectCommand * (^ Between)(id a, id b);

@property (readonly, nonatomic, copy) CoCoDBSelectCommand *With; //不同限定介词的连接词 分隔作用

@end
