//
//  CoCoDBDeleteCommand.h
//  CoCoDatabase
//
//  Created by 陈明 on 2016/5/28.
//  Copyright © 2016年 CoCo. All rights reserved.
//

#import "CoCoDBCommand.h"

@interface CoCoDBDeleteCommand : CoCoDBCommand
@property (readonly, nonatomic, strong) CoCoDBDeleteCommand * (^ All)(void);

@property (readonly, nonatomic, copy) CoCoDBDeleteCommand * (^ Where)(NSString *condition);

@property (readonly, nonatomic, copy) CoCoDBDeleteCommand * (^ And)(NSString *condition);

@property (readonly, nonatomic, copy) CoCoDBDeleteCommand * (^ Or)(NSString *condition);

@property (readonly, nonatomic, copy) CoCoDBDeleteCommand * (^ Between)(id a, id b);

@property (readonly, nonatomic, copy) CoCoDBDeleteCommand *With; //不同限定介词的连接词 分隔作用
@end
