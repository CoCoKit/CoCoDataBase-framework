//
//  CoCoDBCreateDL.h
//  CoCoDatabase
//
//  Created by 陈明 on 2016/5/28.
//  Copyright © 2016年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CoCoDataBaseTableColumn;

@interface CoCoDBCreateDL : NSObject

+ (BOOL)buildTableWithClass:(Class)cls;

+ (NSDictionary *)propertyInDatabaseWithClass:(Class)cls;

+ (NSMutableArray<CoCoDataBaseTableColumn *> *)columnsWithClass:(Class)cls;
+ (NSMutableArray<CoCoDataBaseTableColumn *> *)columnsInDatabaseWithClass:(Class)cls;
@end
