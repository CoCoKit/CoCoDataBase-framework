Pod::Spec.new do |s|
  s.name = "CoCoDataBase"
  s.version = "1.1.0.64"
  s.summary = "CoCoDataBase 用来处理CoCoUI中的一些东西."
  s.license = "MIT"
  s.requires_arc = true
  s.authors = {"iScarlett"=>"iScarlett@qq.com"}
  s.homepage = "https://gitlab.com/CoCoKit"
  s.description = "CoCoDataBase由自动化打包脚本生成提交,由最新push持续集成,保持最新" 
  s.source = { :git => 'https://gitlab.com/CoCoKit/CoCoDataBase-framework.git' }
  s.ios.deployment_target = '8.0'
  s.ios.vendored_framework = 'CoCoDataBase.framework'
  s.dependency 'FMDB' ,'~> 2.6.2'
end
